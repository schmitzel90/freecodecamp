var map;
var apiUrl = "http://api.openweathermap.org/data/2.5/weather?"; 
// INSERT YOUR OWN API KEY
// example "&appid=2348290348209384092384092834098"
var apiKey = "";
var request = "http://api.openweathermap.org/data/2.5/forecast?lat=51.51302&lon=-0.09785&appid=66ad6e716ce4100149b81a1564445703";

//function initmap() {
$(document).ready(function() {
    // map
    var map = L.map('map').setView([51.505, -0.09], 7);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
            maxZoom: 12,
            minZoom: 5,
            id: 'your.mapbox.project.id',
            accessToken: 'your.mapbox.public.access.token'
    }).addTo(map);

    var popup = L.popup();
    var marker = L.marker();

    function onMapClick(e) {
        var coords = e.latlng;
        marker.setLatLng(e.latlng).addTo(map);
        $("#temperatureUnit").html("Temperature [°F] :");
        $(".overlay").css("height", "100%");
        getData(e.latlng);
    }

    function getData(coordsObj) {
        var lat = "lat=" + coordsObj.lat;
        var lon = "&lon=" + coordsObj.lng;
        var apiCallUrl = apiUrl + lat + lon + apiKey;
        $.getJSON(apiCallUrl, function(data) {
            $("#cityName").html(data.name);
            $("#coordinates").html("longitude: " + data.coord.lon + " latitude: " + data.coord.lat);
            $("#icon").attr("src", "http://openweathermap.org/img/w/"+data.weather[0].icon+".png");
            $("#description").html(data.weather[0].description);
            // convert kelvin to fahrenheit
            $("#temperature").html(1.8*(parseInt(data.main.temp)-273)+32);
            $("#wind").html(data.wind.speed);
        });
    }

    map.on('click', onMapClick);
});

function closeOverlay() {
    $(".overlay").css("height", "0%");
}

function changeUnit(elementId) {
    var currentTemp = parseInt($("#temperature").html());
    var currentUnit = $("#temperatureUnit").html();
    if (currentUnit === "Temperature [°F] :") {
        //°C
        $("#temperatureUnit").html("Temperature [°C] :");
        $("#temperature").html(((5/9)*(currentTemp-32)).toFixed());
    } 
    else {
        //°F
        $("#temperatureUnit").html("Temperature [°F] :");
        $("#temperature").html((9/5)*currentTemp+32);
    }

}
