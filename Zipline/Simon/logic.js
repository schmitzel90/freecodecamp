function Button(idButton, enabledButton) {
  var id = idButton;
  var enabled = enabledButton;
  var on = false;

  this.getId = function() {
    return id;
  }
  this.getEnabled = function() {
    return enabled;
  }
  this.getOn = function() {
    return on;
  }
  this.enableButton = function() {
    enabled = true;
  }
  this.disableButton = function() {
    enabled = false;
  }
  this.turnOn = function() {
    if (this.getEnabled()) {
      $("#"+this.getId()).removeClass("buttonHighlighted");
      on = true;
    }
  }
  this.turnOff = function() {
    $("#"+this.getId()).addClass("buttonHighlighted");
    on = false;
  }
}

function ColorButton(idButton, enabledButton, audioFileBtn) {
  Button.call(this, idButton, enabledButton);

  var id = idButton;
  var audioFile = new Audio(audioFileBtn);

  this.highlight = function() {
    if (this.getEnabled()) {
      this.turnOn();
      audioFile.play();
      var oldThis = this;
      setTimeout(function() {
        oldThis.turnOff();
      }, 500);
    }
  }
}

function Simon() {
  var colorQueue = [];
  var playersTurn = false;
  var lastPressedColorButton = undefined;
  var colorQueuePosition = 0;
  var hardMode = false;
  var colorButtons = [
    new ColorButton("1", false, "https://s3.amazonaws.com/freecodecamp/simonSound1.mp3"),
    new ColorButton("2", false, "https://s3.amazonaws.com/freecodecamp/simonSound2.mp3"),
    new ColorButton("3", false, "https://s3.amazonaws.com/freecodecamp/simonSound3.mp3"),
    new ColorButton("4", false, "https://s3.amazonaws.com/freecodecamp/simonSound4.mp3")];
  var startButton = new Button("startButton", false);
  var hardModeButton = new Button("hardModeButton", false);
  var onOffButton = new Button("onOffButton", true);

  this.getColorButtonById = function(id) {
    for (var i = 0; i < colorButtons.length; i++) {
      if (id == colorButtons[i].getId()) {
        return colorButtons[i];
      }
    }
  }

  this.colorButtonClicked = function(buttonId) {
    if (playersTurn) {
      var colorButton = this.getColorButtonById(buttonId);
      colorButton.highlight();
      lastPressedColorButton = buttonId;
      var preThis = this;
      if (this.compareColors(buttonId, colorQueuePosition)) {
        // user wins
        if (colorQueue.length == 20 && colorQueuePosition == colorQueue.length-1) {
          // play winning sequnce
          var i = 1;
          var winSeq = setInterval(function() {
            preThis.winSequence();
            if (i == 2) {
              clearInterval(winSeq);
            }
            i++;
          }, 500);
          this.resetSimon();
          lastPressedColorButton = undefined;
          colorQueuePosition = 0;
        } else if (colorQueue.length-1 == colorQueuePosition) {
          // user succesful clicked queue; new color is added to queue
          colorQueuePosition = 0;
          setTimeout(function() {
            preThis.setNextColor();
          }, 500);
        } else {
          // go to next color in colorQueue
          colorQueuePosition++;
        }
      } else {
        // user pressed the wrong button
        if (hardMode) {
          this.failSequence();
          this.resetSimon();
          setTimeout(function() {
            preThis.setNextColor();
          }, 1000);
        lastPressedColorButton = undefined;
        colorQueuePosition = 0;
        } else {
          playersTurn = false;
          this.failSequence();
          setTimeout(function() {
            preThis.playColorQueue();
          }, 1000);
          lastPressedColorButton = undefined;
          colorQueuePosition = 0;
        }
      }
    }
  }

  this.failSequence = function() {
    for (var i = 0; i < colorButtons.length; i++) {
      (function(j) {
        colorButtons[j].turnOn();
        setTimeout(function() {
          colorButtons[j].turnOff();
        }, 500);
      })(i);
    }
  }

  this.winSequence = function() {
    var i = 0;
    var tmpThis = this;
    var interval = setInterval(function() {
      colorButtons[i].highlight();
      if (i+1 == colorButtons.length) {
        clearInterval(interval);
        playersTurn = true;
      }
      i++;
    }, 600);
  }

  this.turnOnSimon = function() {
    // turn off
    if (onOffButton.getOn()) {
      onOffButton.turnOff();
      startButton.turnOff();
      hardModeButton.turnOff();
      startButton.disableButton();
      hardModeButton.disableButton();
      for (var i = 0; i < colorButtons.length; i++) {
        colorButtons[i].disableButton();
      }
      $("#scoreDisplayText").html("");
      colorQueue = [];
      colorQueuePosition = undefined;
    } else {
      // turn on
      onOffButton.turnOn();
      startButton.enableButton();
      hardModeButton.enableButton();
      $("#scoreDisplayText").html("--");
    }
  }

  this.startSimon = function() {
    if (startButton.getEnabled() && !startButton.getOn()) {
      startButton.turnOn();
      for (var i = 0; i < colorButtons.length; i++) {
        colorButtons[i].enableButton();
      }
      this.setNextColor();
    } else if (startButton.getEnabled()) {
      // restart game
      this.resetSimon();
      var preThis = this;
      setTimeout(function() {
        preThis.setNextColor();
      }, 1000)
    }
  }

  this.setNextColor = function() {
    playersTurn = false;
    this.randomColor();
    this.updateDisplay();
    this.playColorQueue();
  }

  this.startHardMode = function() {
    if (hardModeButton.getOn()) {
      hardModeButton.turnOff();
      hardMode = false;
    } else {
      hardModeButton.turnOn();
      hardMode = true;
    }
  }

  this.resetSimon = function() {
    colorQueue = [];
    this.resetDisplay();
  }

  this.updateDisplay = function() {
    $("#scoreDisplayText").html(colorQueue.length);
  }

  this.resetDisplay = function() {
    $("#scoreDisplayText").html("--");
  }

  this.randomColor = function() {
    var random = Math.random().toFixed(2);
    if (random < 0.25) {
      colorQueue.push(colorButtons[0].getId());
    } else if (random < 0.5) {
      colorQueue.push(colorButtons[1].getId());
    } else if (random < 0.75) {
      colorQueue.push(colorButtons[2].getId());
    } else {
      colorQueue.push(colorButtons[3].getId());
    }
  }

  // compare pressed colorButton with an element from colorQueue
  this.compareColors = function(idButton, colorQueuePosition) {
    if (idButton === colorQueue[colorQueuePosition]) {
      return true;
    } else {
      return false;
    }
  }

  this.playColorQueue = function() {
    var i = 1;
    var tmpThis = this;
    var interval = setInterval(function() {
      var tmpColorButtonObject = tmpThis.getColorButtonById(colorQueue[i-1]);
      tmpColorButtonObject.highlight();
      if (i == colorQueue.length) {
        clearInterval(interval);
        playersTurn = true;
      }
      i++;
    }, 800);
  }
}

var simonGame1 = new Simon();
