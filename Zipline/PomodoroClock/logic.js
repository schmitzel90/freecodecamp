var min, sec;
var sessionValue;
var breakValue;

var isSession = true; // true => sessionMin, false => breakMin
var valueFrom = "#sessionMin"; // possible values: #sessionMin, #breakMin
var intervalId; // value set by setInterval in startTimer()
var startTimeSession;
var startTimeBreak;

$(document).ready(function() {
    sessionValue = parseInt(document.getElementById("sessionMin").innerHTML);
    breakValue = parseInt(document.getElementById("breakMin").innerHTML);

    // variables used to for progressBar
    startTimeSession = sessionValue;
    startTimeBreak = breakValue;

    sessionOrBreak();
})

/*
    changeMinute Function
    --> update timer for either session or break timer
    Parameter:
        val = change Value, either +1 or -1
        id = span element id, either sessionMin or breakMin
*/
function changeMinute(val, id) {
    // get minute from display
    var currentDisplayTime = parseInt(document.getElementById(id).innerHTML);

    // change ui to default
    document.getElementById("timer").innerHTML =  "--";

    // prevent display negative numbers
    if (currentDisplayTime + val > 0) {
        currentDisplayTime += val;
        document.getElementById(id).innerHTML = currentDisplayTime;
    }

    // update variable
    sessionValue = parseInt(document.getElementById("sessionMin").innerHTML);
    breakValue = parseInt(document.getElementById("breakMin").innerHTML);

    // TODO
    startTimeSession = sessionValue;
    startTimeBreak = breakValue;

    sessionOrBreak();
    isSession = true;
}

function startTimer() {

    //start countdown
    intervalId = setInterval(countDown, 1000);

    //disable increase decrease buttons
    disableButton("#sessionMinDown");
    disableButton("#sessionMinUp");
    disableButton("#breakMinUp");
    disableButton("#breakMinDown");
    //disable start button
    disableButton("#startBtn");
    //enable pause button
    enableButton("#pauseBtn");
}

function stopCountDown() {

    //stop countdown
    clearInterval(intervalId);

    //enable increase, decrease button
    enableButton("#sessionMinDown");
    enableButton("#sessionMinUp");
    enableButton("#breakMinUp");
    enableButton("#breakMinDown");
    //enable start button
    enableButton("#startBtn");
    //disable pause button
    disableButton("#pauseBtn");

    // change to default background color
    $(".pomodoro").css("background-color", "#5dae8b");
}

//set min value correctly and reset sec
function sessionOrBreak() {
    if (isSession) { //session
        min = sessionValue;
    } else { //break
        min = breakValue;
    }
    sec = 0;
}

function countDown() {
    if (sec === 0 && min > 0) { //decrease minute
        sec = 59;
        min--;
    } else { //decrease second
        sec--;
    }

    //update userinterface
    document.getElementById("timer").innerHTML = min + "  :  " + sec;

    updateProgressBar();

    //stop timer at 0 : 0
    if (min === 0 && sec === 0) {
        // session is over, take a break
        if (isSession) {
            isSession = false;
            sessionOrBreak();
            $(".pomodoro").css("background-color", "#ff7676");
        }
        // break is over, get back to work
        else {
            isSession = true;
            sessionOrBreak();
            $(".pomodoro").css("background-color", "#5dae8b");
        }
    }
}

//add class to disable button
function disableButton(id) {
    $(id).addClass("disabled");
}

//remove class to enable button
function enableButton(id) {
    $(id).removeClass("disabled");
}

function updateProgressBar() {
    if (isSession) {
        $("#progressBar").css("width", ((min*60+sec)/(startTimeSession*60)*100).toFixed(2)+"%");
    }
    else {
        // fill progressBar reverse when break
        $("#progressBar").css("width", parseInt(100-((min*60+sec)/(startTimeSession*60)*100).toFixed(2))+"%");
    }
}
