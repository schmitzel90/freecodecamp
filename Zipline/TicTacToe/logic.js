
/*
 * ----------------------------------------------------------------------------
 *  FieldElement Object
 * ----------------------------------------------------------------------------
 */
function FieldElement(fieldId) {
    this.id = fieldId;
    this.occupied = false;
}

FieldElement.prototype = {
    getId: function() {
        return this.id;
    },
    getOccupied: function() {
        return this.occupied;
    },
    setOccupied: function(condition) {
        this.occupied = condition;
    }
};

/*
 * ----------------------------------------------------------------------------
 *  PlayingField Object
 * ----------------------------------------------------------------------------
 */
function PlayingField() {
    this.fieldElements = [];
    this.addFieldElements();
}

PlayingField.prototype = {
    addFieldElements: function() {
        for (var i = 1; i <= 3*3; i++) {
            this.fieldElements.push(new FieldElement(i));
        }
    },
    getFieldElementById: function(fieldId) {
        for (var i = 0; i < this.fieldElements.length; i++) {
            if (parseInt(fieldId) === this.fieldElements[i].getId()) {
                return this.fieldElements[i];
            }
        }
        return undefined;
    },
    // all fieldElements are no longer occuppied
    resetFieldElements: function() {
        for (var i = 0; i < this.fieldElements.length; i++) {
            this.fieldElements[i].setOccupied(false);
            $("h2").remove();
        }
    },
    isFieldElementOccupied: function(fieldId) {
        var tmpFieldElement = this.getFieldElementById(fieldId);
        if (tmpFieldElement.getOccupied()) {
            return true;
        } else {
            return false;
        }
    },
    setFieldElementOccupied: function(fieldId, symbol) {
        var tmpFieldElement = this.getFieldElementById(fieldId);
        tmpFieldElement.setOccupied(true);
        $("#"+fieldId).append("<h2 id='1" + fieldId +"'>" + symbol + "</h2>");
    },
    // check if all fieldElements are occupied
    checkAllOccupy: function() {
        for (var i = 0; i < this.fieldElements.length; i++) {
            if (!this.fieldElements[i].getOccupied()) {
                return false;
            }
        }
        return true;
    },
    highlightWinningFieldElements: function(winArray) {
        for (var i = 0; i < winArray.length; i++) {
            $("#1" + winArray[i]).css("color", "red");
        }
    }
};

/*
 * ----------------------------------------------------------------------------
 *  Player Object
 * ----------------------------------------------------------------------------
 */
function Player(playerSymbol) {
    this.symbol = playerSymbol;
    this.winCombinations =  [[1,2,3],[4,5,6],[7,8,9],   // horizontal
                            [1,4,7],[2,5,8],[3,6,9],    // vertical
                            [1,5,9],[3,5,7]];           // diagonal
    this.queue = [];
}

Player.prototype = {
    getSymbol: function() {
        return this.symbol;
    },
    getQueue: function() {
        return this.queue;
    },
    updateQueue: function(fieldId) {
        this.queue.push(parseInt(fieldId));
    },
    // check if player has got 3 fieldElements of a winCombination
    isWinner: function() {
        for (var i = 0; i < this.winCombinations.length; i++) {
            var tmpCounter = 0;
            for (var j = 0; j < this.winCombinations[i].length; j++) {
                if (this.getQueue().indexOf(this.winCombinations[i][j]) != -1) {
                    tmpCounter++;
                    if (tmpCounter === 3) {
                        return this.winCombinations[i];
                    }
                } 
                else {
                    break;
                }
            }
        }
        return undefined;
    },
    resetQueue: function() {
        this.queue = [];
    },
    setSymbol: function(newSymbol) {
        this.symbol = newSymbol;
    }
};

/*
 * ----------------------------------------------------------------------------
 *  Computer Object
 *  -> inherits from Player Object
 *  Computer is stupid, he only can set symbols randomly
 *  --> maybe later on i might implement a clever algorithm (minimax-algorithm)
 * ----------------------------------------------------------------------------
 */
function Computer(computerSymbol) {
    Player.call(this, computerSymbol);
}

Computer.prototype = Object.create(Player.prototype);
Computer.prototype.constructor = Computer;

Computer.prototype.createRandomFieldElementId = function() {
    return Math.floor(Math.random() * 9 + 1);
}

/*
 * ----------------------------------------------------------------------------
 *  TicTacToe Object
 * ----------------------------------------------------------------------------
 */
function TicTacToe() {
    this.player = new Player("O");
    this.computer = new Computer("X");
    this.playingField = new PlayingField();
    this.playersTurn = true;
}

TicTacToe.prototype = {
    playerClickedFieldElement: function(fieldId) {
        if (this.playersTurn) {
            if (!this.playingField.isFieldElementOccupied(fieldId)) {
                this.player.updateQueue(fieldId);
                this.playingField.setFieldElementOccupied(fieldId, this.player.getSymbol());
                this.playersTurn = false;

                var tttThis = this;
                if (this.player.isWinner() != undefined) {
                    // winner
                    this.playingField.highlightWinningFieldElements(this.player.isWinner());
                    setTimeout(function() {
                        tttThis.startNewGame();
                    }, 1000);
                } 
                else if (this.playingField.checkAllOccupy()) {
                    // all fieldElements are occupied
                    setTimeout(function() {
                        tttThis.startNewGame();
                    }, 1000);
                } 
                else {
                    // computer's turn
                    this.computerClickedFieldElement();
                }
            }
        }
    },
    computerClickedFieldElement: function() {
        var fieldId = this.computer.createRandomFieldElementId();
        // get new id if it is occupied
        while (this.playingField.isFieldElementOccupied(fieldId)) {
            fieldId = this.computer.createRandomFieldElementId();
        }
        this.computer.updateQueue(fieldId);
        this.playingField.setFieldElementOccupied(fieldId, this.computer.getSymbol());
        // check if player wins the game
        var tttThis = this;
        if (this.computer.isWinner() != undefined) {
            // winner
            this.playingField.highlightWinningFieldElements(this.computer.isWinner());
            setTimeout(function() {
                tttThis.startNewGame();
            }, 1000);
        } 
        else if (this.playingField.checkAllOccupy()) {
            // all fieldElements are occupied
            setTimeout(function() {
                tttThis.startNewGame();
            }, 1000);
        } 
        else {
            // computer's turn
            this.playersTurn = true;
        }
    },
    startNewGame: function() {
        this.playingField.resetFieldElements();
        this.player.resetQueue();
        this.computer.resetQueue();
        // computer sets first symbol in the new game
        this.computerClickedFieldElement();
    },
    chooseSymbol: function(newSymbol) {
        if (newSymbol === "X") {
            this.player.setSymbol("X");
            this.computer.setSymbol("O");
        }
        document.getElementById("myNav").style.height = "0%";
        ticTacToe.computerClickedFieldElement();
    }
};

/*
 * ----------------------------------------------------------------------------
 *  START GAME
 * ----------------------------------------------------------------------------
 */
var ticTacToe = new TicTacToe();
