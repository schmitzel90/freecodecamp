var channels = [
    "freecodecamp", "storbeck", "terakilobyte", "habathcx",
    "RobotCaleb", "thomasballinger", "noobs2ninjas", "beohoff", "medrybw",
    "RocketBeansTV"
];
var channelOnline = new Array();
var channelOffline = new Array();

$(document).ready(function() {
    channels.forEach(function(channel) {
        // check if stream is online; does not provide enough information about the stream if offline
        $.getJSON("https://api.twitch.tv/kraken/streams/" + channel + "?callback=?", function(streamData) {
            // get information about the channel
            // unfortunately you do not get any information if the stream is online
            // reason for sending two requests
            $.getJSON("https://api.twitch.tv/kraken/channels/" + channel + "?callback=?", function(channelData) {
                if (streamData.stream != null) {
                    // add property
                    channelData.onlineStatus = "online";
                    channelOnline.push(channelData);
                } else {
                    // add property
                    channelData.onlineStatus = "offline";
                    channelOffline.push(channelData);
                }
            });
        });
    });

    $("#searchChannel").keyup(function() {
        $(".offline").remove();
        $(".online").remove();
        var pattern = $("#searchChannel").val();
        var localChannelList = channelOffline.concat(channelOnline);
        for (var i = 0; i < localChannelList.length; i++) {
            if (localChannelList[i].name.search(pattern) != -1) {
                displayChannel(localChannelList[i]);
            }
        }
    });

    setTimeout(function() {
        showChannel("0");
    }, 2000);
});

function showChannel(buttonId) {
    $(".offline").remove();
    $(".online").remove();
    var localChannelList = [];
    // show all channels
    switch (buttonId) {
        case "0":
            localChannelList = channelOffline.concat(channelOnline);
            break;
        case "1":
            localChannelList = channelOnline;
            break;
        case "2":
            localChannelList = channelOffline;
            break;
    }
    for (var i = 0; i < localChannelList.length; i++) {
        displayChannel(localChannelList[i]);
    }
}

function displayChannel(channel) {
    var logo = undefined;
    if (channel.logo != null) {
        logo = channel.logo;
    }
    else {
        logo = "http://www.caesar.it/images/prodotti/solid-color-grey.jpg"
    }
    $("tbody").prepend(
            "<tr class='" + channel.onlineStatus + "'>" +
            "<td>" +
            "<a target='_blank' href='" + channel.url + "'>" +
            "<img class='img-rounded' src='" + logo + "'>" +
            "</a>" +
            "</td>" +
            "<td>" + channel.display_name + "</td>" +
            "<td>" + channel.status + "</td>" +
            "<td id='" + channel.onlineStatus +"'>" + channel.onlineStatus +"</td>" +
            "</tr>");
}
